BLERALD Clément 

SANGSIRI Nicolas

# Projet de Langages à objets avancés M1 : Circuit combinatoire (sujet 2)

## Instructions de compilation et d'exécution

- Ouvrir un terminal dans le répertoire `Code` et entrer `make`
- Entrer `./Prog` pour exécuter le programme 
- Suivre les instructions à l'écran


Pour d'amples détails sur la modélisation et conception du programme, veillez lire le `Rapport.pdf`
