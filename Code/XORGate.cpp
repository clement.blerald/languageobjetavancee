#include "XORGate.hpp"

XORGate::XORGate(Gate *m_fg, Gate *m_fd) : Gate2Input{m_fg,m_fd}{
    label = "XOR";
    type = 8;
    if(fd != nullptr && fg != nullptr) setVal();
}

void XORGate::setVal(){
    val = fg->getVal() != fd->getVal(); 
}