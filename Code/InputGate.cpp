#include "InputGate.hpp"

InputGate::InputGate(string label): GateNoInput{label}{
    type = 1;
    val = 0;
    color = true;
}

void InputGate::setVal(){
    val = (val == 0 ) ? 1 : 0;
}

int InputGate::getType() const{
    return type;
}