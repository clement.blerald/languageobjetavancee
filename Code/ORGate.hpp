#include <iostream>
#include <string>
#include "Gate2Input.hpp"

using namespace std;

#ifndef ORGATE_HPP
#define ORGATE_HPP

class ORGate : public Gate2Input{
    public:
        ORGate(Gate *fg, Gate *fd);
        void setVal() override;
};

#endif