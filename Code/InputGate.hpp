#include <iostream>
#include <string>
#include "GateNoInput.hpp"

using namespace std;

#ifndef INPUTGATE_HPP
#define INPUTGATE_HPP

class InputGate : public GateNoInput{
    public:
        InputGate(string label);
        void setVal() override;
        int getType() const override;
        void changeInputVal();
};



#endif