#include "NOTGate.hpp"

NOTGate::NOTGate(Gate *fg) : Gate1Input{fg} { 
    label = "NOT";
    type = 4;
    if(fg != nullptr) setVal();
}

void NOTGate::setVal() {
    val = !fg->getVal();
}