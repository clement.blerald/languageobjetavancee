#include "Gate.hpp"

class Node{

friend class Arbre;
friend class ArbreView;
friend class Shell;

    private:
        Gate *gate;
        int profondeur;
        int x, y;

    public:
        Node();
        Node(Gate *gate);
        Node(Gate *gate, int profondeur);
        
        void setX(int val);
        void setY(int val);
        int getX() const;
        int getY() const;

        virtual ~Node();
};