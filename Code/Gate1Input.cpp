#include "Gate1Input.hpp"

Gate1Input::Gate1Input(Gate *m_fg) : Gate{m_fg} {}

Gate1Input::~Gate1Input(){
    if(fg!= nullptr) delete fg;
}

Gate * Gate1Input::setSon(Gate * other){
    if(fg == nullptr){
        fg = other;
        return this;
    } 
    cout << "couldn't setSon properly" << endl;
    return this;
}

int Gate1Input::getType() const{
    return type;
}