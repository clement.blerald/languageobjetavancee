#include <iostream>
#include <string>
#include "Gate2Input.hpp"

using namespace std;

#ifndef XORGATE_HPP
#define XORGATE_HPP

class XORGate : public Gate2Input{
    public:
        XORGate(Gate *fg, Gate *fd);
        void setVal() override;
};



#endif