#include "Shell.hpp"
using namespace std;

#ifndef PARSERCOMMANDLINE_HPP
#define PARSERCOMMANDLINE_HPP

class ParserCommandLine{
    public:
        ParserCommandLine();
        virtual ~ParserCommandLine();
        static void run();
};



#endif