#include <iostream>
#include <string>
#include <regex>
#include <algorithm>

using namespace std;

#ifndef Gate_HPP
#define Gate_HPP

class Gate{
    friend class Shell;
    friend class Arbre;
    friend class ParserFic;

    protected:
        //attributes
        string label;
        Gate *fg;
        Gate *fd;
        bool val, color;        
        int type;


        //ctors
        Gate(string m_label); //for InputGate 
        Gate(Gate *fg); //for NOTGate and OutputGate
        Gate(Gate *fg, Gate *fd);//for all the others

        //To override 
        virtual void setVal() = 0;
        virtual Gate * setSon(Gate * other) = 0;
        
        void fillLabels(int length);
        void changeInputValue(string input_name, regex e, vector<Gate*> &changed);
        void majVal();
        void setLabel(int length);

    public:
        
        virtual ~Gate();
        virtual int getType() const = 0;
        bool equals(Gate * other);
        void colorizeGates(vector<string> list,bool color);
        void saveTo(string &circuit, regex e);

        //getters
        bool getVal() const;
        string getLabel() const;
        bool getColor() const;
        Gate *getFg() const;
        Gate *getFd() const;
        Gate *getGate();
        void setColor(bool color);
};

#endif