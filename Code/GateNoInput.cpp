#include "GateNoInput.hpp"

GateNoInput::GateNoInput(string m_label) : Gate{m_label} {}

Gate * GateNoInput::setSon(Gate * other){
    cout << "this Gate have no son" << endl;
    return this;
}