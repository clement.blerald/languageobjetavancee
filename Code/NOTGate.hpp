#include <iostream>
#include <string>
#include "Gate1Input.hpp"

using namespace std;

#ifndef NOTGATE_HPP
#define NOTGATE_HPP

class NOTGate : public Gate1Input{
    public:
        NOTGate(Gate *fg);
        void setVal() override;
};



#endif