#include "Gate.hpp"
#include "ANDGate.hpp"
#include "NANDGate.hpp"
#include "NORGate.hpp"
#include "NOTGate.hpp"
#include "NXORGate.hpp"
#include "ORGate.hpp"
#include "XORGate.hpp"
#include "InputGate.hpp"
#include "OutputGate.hpp"
#include "ParserCommandLine.hpp"

int main(int argc, char **argv){
    ParserCommandLine shl;
    shl.run();
    return 0;
}