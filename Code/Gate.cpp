#include "Gate.hpp"

// output
Gate::Gate(Gate *m_fg)
    : fg{m_fg}, fd{nullptr} , color{false}{}

Gate::Gate(Gate *m_fg, Gate *m_fd)
    : fg{m_fg}, fd{m_fd} , color{false}{}

Gate::Gate(string m_label) : label{m_label}, fg{nullptr}, fd{nullptr}, color{false} {}

Gate::~Gate() {
    if(fg != nullptr) delete fg;
    if(fd != nullptr) delete fd;
}

bool Gate::equals(Gate *other) {
    if (type != other->type) return false;
    if (type == 1 && other->type == 1) return label == other->label;
    
    bool left = true, right = true;

    if (fg != nullptr && other->fg != nullptr)
        left = fg->equals(other->fg);

    if (fd != nullptr && other->fd != nullptr)
        right = fd->equals(other->fd);

    return left && right;
}

void Gate::majVal(){
    if(fg != nullptr) fg->majVal();
    if(fd != nullptr) fd->majVal();

    if(type != 1)
        setVal();
}

void Gate::fillLabels(int length){
    if(type != 7)
        setLabel(length);

    if(fg != nullptr) fg->fillLabels(length);
    if(fd != nullptr) fd->fillLabels(length);
}

void Gate::setLabel(int length) {
    int diff = length - label.length();
    size_t left = diff / 2;
    size_t right = diff % 2 == 0 ? left : left + 1;
    string motif = (type == 1) ? " " : "_";

    for (size_t i = 0; i < left; i++) label = motif + label;
    for (size_t i = 0; i < right; i++) label = label + motif;
}

void Gate::colorizeGates(vector<string> list, bool m_color){
    if(find(list.begin(), list.end(), label) != list.end())
        color = m_color;

    if(fg != nullptr) fg->colorizeGates(list, m_color);
    if(fd != nullptr) fd->colorizeGates(list, m_color);
}

void Gate::changeInputValue(string input_name, regex e, vector<Gate*> &changed){
    string lab = regex_replace(label,e,"");
    if(type == 1 && ((lab.compare(input_name)) == 0) && find(changed.begin(), changed.end(), this) == changed.end()){
        setVal();
        changed.push_back(this);
        return;
    }
        
    if(fg != nullptr) fg->changeInputValue(input_name,e,changed);
    if(fd != nullptr) fd->changeInputValue(input_name,e,changed);
}

void Gate::saveTo(string &circuit, regex e){
    circuit += regex_replace(label,e,"");
    
    if(type != 1)
        circuit += "(";
    
    if(fg != nullptr)
        fg->saveTo(circuit,e);
    
    if(type != 1)
        circuit += ",";

    if(fd != nullptr)
        fd->saveTo(circuit,e);
    
    if(type != 1)
        circuit += ")";
}

//getters and setters
bool Gate::getVal() const { return val; }
string Gate::getLabel() const { return label; }
Gate *Gate::getGate() { return this;}
Gate *Gate::getFd() const{return fd;}
Gate *Gate::getFg() const{return fg;}
bool Gate::getColor() const { return color; }
void Gate::setColor(bool nv_color){ color = nv_color;}
