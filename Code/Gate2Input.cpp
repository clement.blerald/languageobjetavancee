#include "Gate2Input.hpp"

Gate2Input::Gate2Input(Gate *m_fg, Gate *m_fd) : Gate{m_fg,m_fd} {}

Gate * Gate2Input::setSon(Gate * other){
    if(fg == nullptr){
        fg = other;
        return this;
    } else if(fd == nullptr){
        fd = other;
        return this;
    } 
    cout << "couldn't setSon properly" << endl;
    return this;
}

int Gate2Input::getType() const{
    return type;
}