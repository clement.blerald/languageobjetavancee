#include <iostream>
#include <string>
#include "Gate2Input.hpp"

using namespace std;

#ifndef NXORGATE_HPP
#define NXORGATE_HPP

class NXORGate : public Gate2Input{
    public:
        NXORGate(Gate *fg, Gate *fd);
        void setVal() override;
};



#endif