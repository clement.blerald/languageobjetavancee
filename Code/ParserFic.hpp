#include <iostream>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <regex>
#include <stack>

#include "ANDGate.hpp"
#include "NANDGate.hpp"
#include "NORGate.hpp"
#include "NOTGate.hpp"
#include "NXORGate.hpp"
#include "ORGate.hpp"
#include "XORGate.hpp"
#include "InputGate.hpp"
#include "OutputGate.hpp"
#include "Arbre.hpp"

#ifndef PARSERFIC_HPP
#define PARSERFIC_HPP

class ParserFic{
    friend class ParserCommandLine;
    public:
        static Arbre *parseFic(const string pathname);
    private:
        static Arbre *buildArbre(string output_line, Arbre *tree);
        static void build(string expression, Gate *racine, Arbre *arbre);
        static Gate * createGate(const string expression);
};

#endif