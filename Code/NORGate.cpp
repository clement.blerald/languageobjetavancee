#include "NORGate.hpp"

NORGate::NORGate(Gate *fg, Gate *fd) : Gate2Input{fg,fd} {
    label = "NOR";
    type = 3;
    if(fd != nullptr && fg != nullptr) setVal();
}

void NORGate::setVal(){
    val = !fg->getVal() && !fd->getVal();
}