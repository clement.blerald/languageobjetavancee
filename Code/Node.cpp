#include "Node.hpp"

Node::Node() : gate{nullptr}, x{0}, y{0}{}
Node::Node(Gate *_gate) : gate{_gate}, profondeur(0), x{0}, y{0}{}
Node::Node(Gate *_gate, int _profondeur) : gate{_gate}, profondeur(_profondeur), x{0}, y{0}{}

Node::~Node(){
    delete gate;
}

void Node::setX(int val){
    x = val;
}

void Node::setY(int val){
    y = val;
}

int Node::getX() const{
    return x;
}

int Node::getY() const{
    return y;
}