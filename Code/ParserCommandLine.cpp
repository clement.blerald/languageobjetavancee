#include "ParserCommandLine.hpp"

ParserCommandLine::ParserCommandLine(){}
ParserCommandLine::~ParserCommandLine(){}

void ParserCommandLine::run(){

    char cmd;
    while(true){
        cout << "Enter f to give a path to circuit file or t to type an expression or m for default circuit:" << endl;
        cin >> cmd;
        cmd = tolower(cmd);     
        if(cmd == 'f' || cmd == 'm' || cmd == 't') break;
    }

    cin.ignore();//clear input buffer pour le getline

    if(cmd == 'f'){
        Arbre *a1 = nullptr;
        while(a1 == nullptr){
            cout << "Please enter path to circuit file:" << endl;
            string filepath;
            getline(cin,filepath);
            a1 = ParserFic::parseFic(filepath);
        }
        ArbreView *a4 = new ArbreView(a1);
        Shell s{a4};
    } else if(cmd == 't'){
        while(true){
            cout << "Please enter a circuit expression :" << endl;
            string circ;
            getline(cin,circ);
            try{
                Arbre *a = new Arbre{};
                a = ParserFic::buildArbre(circ,a);
                a->setDepth();
                a->setLabel_length();
                ArbreView *view = new ArbreView(a);
                Shell sh{view};
                return;
            }catch(const std::exception& e){
                cout << "Your circuit expression is incorrect\n" << endl;
            }
        }
    } else {
        Gate *a = new InputGate ("a");
        Gate *b = new InputGate ("b");
        Gate *or1 = new ORGate(a, b);
        Gate *and1 = new ANDGate(a ,b);
        Gate *xor1 = new XORGate(and1, or1);
        OutputGate * A = new OutputGate("A",xor1);
        Arbre *a2 = new Arbre(A);
        ArbreView *a3 = new ArbreView(a2);
        Shell s{a3};
    }
}