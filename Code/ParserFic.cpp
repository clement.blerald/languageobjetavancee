#include "ParserFic.hpp"

Gate *ParserFic::createGate(const string expr){
    if(expr == "xor") return new XORGate(nullptr,nullptr);
    if(expr == "or") return new ORGate(nullptr,nullptr);
    if(expr == "and") return new ANDGate(nullptr,nullptr);
    if(expr == "nor") return new NORGate(nullptr,nullptr);
    if(expr == "not") return new NOTGate(nullptr);
    if(expr == "nxor") return new NXORGate(nullptr,nullptr);
    if(expr.length() == 1 && isalpha(expr[0])) return new InputGate(expr);

    return nullptr;
}

void ParserFic::build(string expr, Gate *racine, Arbre *arbre) {
    if (expr == "") return;

    regex reg{"([a-z]+)"};
    smatch m;

    sregex_iterator current{expr.begin(), expr.end(),reg};
    sregex_iterator lastMatch;

    vector<string> gate_list;

    while(current != lastMatch) {
        m = *current;
        gate_list.push_back(m.str());
        current++;
    }

    stack<Gate *> stack;
    for(int i = gate_list.size() - 1 ; i >= 0 ; i--){
        Gate *nvGate = createGate(gate_list[i]);
        if(nvGate == nullptr)
            throw invalid_argument("");
        Gate *tmp = nullptr;
        if(nvGate->getType() != 1){
            Gate *fg = stack.top();
            stack.pop();

            if((tmp = arbre->existingNode(fg)) == nullptr){
                arbre->list_node.push_back(new Node(fg));
            }
            
            nvGate->setSon((tmp != nullptr ? tmp : fg));            
            
            if(nvGate->getType() != 4){
                Gate *fd = stack.top();
                stack.pop();

                if((tmp = arbre->existingNode(fd)) == nullptr){
                    arbre->list_node.push_back(new Node(fd));
                }
                nvGate->setSon((tmp != nullptr ? tmp : fd));
            }
        }

        if((tmp = arbre->existingNode(nvGate)) == nullptr){
            arbre->list_node.push_back(new Node(nvGate));
        }
        stack.push((tmp != nullptr ? tmp : nvGate));
    }
    racine->setSon(stack.top());
}

Arbre *ParserFic::buildArbre(string expression, Arbre *arbre) {
    string delimiter = "=";
    string output_label = expression.substr(0,expression.find(delimiter));
    // toute l'expression sans l'output
    expression = expression.substr(output_label.length() + 1); 

    // remove whitespace
    output_label.erase(remove(output_label.begin(), output_label.end(), ' '),
                       output_label.end());

    Gate *racine = new OutputGate(output_label, nullptr); 
    arbre->list_node.push_back(new Node(racine));

    build(expression, racine,arbre);
    
    arbre->list_racine.push_back(racine);
    return arbre;
}

Arbre *ParserFic::parseFic(const string pathname) {
    ifstream file;
    file.open(pathname);

    if (!file.is_open()) {
        cout << "Failed to open" << endl;
        return nullptr;
    }

    stringstream buffer;
    buffer << file.rdbuf();
    string content = buffer.str();

    Arbre *arbre = new Arbre();

    string token;
    string delimiter = "\n";
    size_t pos = 0;

    // for each output
    while ((pos = content.find(delimiter)) != string::npos) {
        token = content.substr(0, pos);
        arbre = buildArbre(token, arbre);

        content.erase(0, pos + delimiter.length());
    }
      
    arbre->setDepth();
    arbre->setLabel_length();
    file.close();
    
    return arbre;
}