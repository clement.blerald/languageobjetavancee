#include "ArbreView.hpp"
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>  

#ifndef SHELL_HPP
#define SHELL_HPP

class Shell{
    private:
        ArbreView* view;
        
        void run();
        void print_man();
        
        void getInputsGate(vector<string> &input_list);
        bool inputsListener(vector<string> &input_list, char cmd);
        void saveTo(const string pathname, Arbre *arbre);
    
    public:
        Shell(ArbreView* m_view);
        virtual ~Shell();
    
};

#endif