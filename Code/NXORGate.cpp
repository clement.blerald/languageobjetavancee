#include "NXORGate.hpp"

NXORGate::NXORGate(Gate *fg, Gate *fd) : Gate2Input{fg,fd}{
    label = "NXOR";
    type = 5;
    if(fd != nullptr && fg != nullptr) setVal();
}

void NXORGate::setVal(){
    val = fg->getVal() == fd->getVal(); 
}