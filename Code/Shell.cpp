#include "Shell.hpp"

Shell::Shell(ArbreView *m_view) : view{m_view}{
    run();
}

Shell::~Shell() {}

void Shell::getInputsGate(vector<string> &input_list){
    for(Node *n: view->tree->list_node){
        if(n->gate->getType() == 1){
            string label = n->gate->getLabel();
            input_list.push_back(label);
        }
    }
}

bool Shell::inputsListener(vector<string> &input_list, char cmd){
    regex e("^\\s+|\\s+$");
    for(string str : input_list){
        string label = regex_replace(str,e,"");
        
        //char to string 
        string scmd;
        scmd.push_back(cmd); 
        
        if((label.compare(scmd)) == 0){
            view->tree->changeInputValue(scmd,e);
            return true;
        }
    }
    return false;
}

void Shell::saveTo(const string pathname, Arbre *tree){
    regex e("^\\s+|\\s+$");
    string circuit;

    tree->list_racine[0]->fg->saveTo(circuit,e);
    transform(circuit.begin(), circuit.end(), circuit.begin(), ::tolower);
    circuit = tree->list_racine[0]->getLabel() + " = " + circuit + "\n";
    
    ofstream outfile(pathname);

    outfile << circuit;
    outfile.close();
} 

void Shell::run(){
    char cmd;
    view->display();

    vector<string> input_list;
    getInputsGate(input_list);
    
    while(true){
        cout << "$ (please enter your command or man for help) : " << view->current->gate->getLabel() << endl;
       
        scanf(" %c", &cmd);
        system("clear");
        
        if(cmd == 'x')
            return;
        else if(cmd == 'm'){
            print_man();
        }else if(cmd == 'z' || cmd == 'q' || cmd == 's' || cmd == 'd'){
            view->tree->MajVal();
            view->moveCursor(cmd);
        }else if(inputsListener(input_list,cmd)){
            view->tree->MajVal();
            view->placeGate(); 
        }else if(cmd == '+'){
            string pathname = "\n";
            while(pathname == "" || pathname == "\n"){
                if(pathname == "\n")
                    cout << "Veuillez entrer un nom de fichier : " << endl;
                getline(cin,pathname);
            }
                
            saveTo(pathname + ".txt",view->tree);
            system("clear");
        }
        else
            cout << "Unknown command : |" << cmd << "|,type man to see all availabes commands" << endl;
        
        if (cmd != '\n')
            cout << endl;
        
        view->display();
    }
}

void Shell::print_man(){
    system("clear");
    cout << "To go left => \"q\"" << endl;
    cout << "To go right => \"d\"" << endl;
    cout << "To go up => \"z\"" << endl;
    cout << "To go down => \"s\"" << endl;
    cout << "To quit => \"x\"" << endl << endl;
}