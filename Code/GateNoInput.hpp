#include <iostream>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>

#include "Gate.hpp"

#ifndef GATENOINPUT_HPP
#define GATENOINPUT_HPP

class GateNoInput : public Gate{
    protected:
        GateNoInput(string m_label);
        virtual int getType() const = 0;
        virtual void setVal() = 0;
        Gate * setSon(Gate * other) override;
};

#endif