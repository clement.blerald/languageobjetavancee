#include <iostream>
#include <string>
#include "Gate2Input.hpp"

#ifndef NORGATE_HPP
#define NORGATE_HPP

class NORGate : public Gate2Input{
    public:
        NORGate(Gate *fg, Gate *fd);
        void setVal() override;
};

#endif