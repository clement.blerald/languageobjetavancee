#include "ArbreView.hpp"

ArbreView::ArbreView(Arbre *&m_tree) : tree{m_tree} {
    setGrid();
    current = initCursorPosition();
    placeGate();
    placeWire();
}

ArbreView::~ArbreView() {}

Node* ArbreView::initCursorPosition(){
    for(size_t i = 0; i < tree->list_node.size(); i++){
        Node *node = tree->list_node[i];
        if(node->gate->getType() == 1 && node->profondeur == tree->profondeur) return node;
    }
    return tree->list_node[0];
}

void ArbreView::setGrid() {
    int i = 1;
    size_t nbr_gate_max = 0;
    height = 2;
    width = 0;
    vector<Node *> floor = tree->getNodes(i);
    while (floor.size() != 0) {
        height += (floor.size() * 2) + 2;
        if (floor.size() > nbr_gate_max) nbr_gate_max = floor.size();
        floor = tree->getNodes(++i);
    }
  
    floor = tree->getNodes(i - 1);
    width = (nbr_gate_max * tree->label_length) + nbr_gate_max - 1;
    
    vector<vector<string>> vec(height, vector<string>(width, " "));
    grid = vec;
    this->nbGateMax = nbr_gate_max;
 }

void ArbreView::placeGate() {
    int i = 0, decalage = 0, x = height - 1, y = 0;
    vector<Node *> floor = tree->getNodes(i++);
    
    while (floor.size() != 0) {
        decalage = (width - (tree->label_length * floor.size() + (floor.size() - 1))) / 2;
        for (Node *n : floor) {
            string label = n->gate->getLabel();
            decalage += (tree->label_length - label.size()) / 2;

            for (size_t idx = 0; idx < label.size(); idx++) {
                if (n->gate->getColor()) {
                    string left = (n->gate->getVal() ? "\033[1;32m" : "\033[1;31m");
                    string right = "\033[0m";
                    grid[x][y + decalage + idx] = left + label[idx] + right;
                } else
                    grid[x][y + decalage + idx] = label[idx];
            }
            n->setX(x);
            n->setY(y + decalage + (tree->label_length / 2));
            y += tree->label_length + 1;
        }

        y = 0;
        floor = tree->getNodes(i++);
        if (i == 2)
            x -= 2;
        else
            x -= (floor.size() * 2) + 2;
    }
}

void ArbreView::getSonsCoord(Node *n, vector<tuple<int, int, int>> &coords) {
    coords.clear();

    for (Node *node : tree->list_node) {
        if ((n->gate->getFg() != nullptr && node->gate->equals(n->gate->getFg())) ||
            (n->gate->getFd() != nullptr && node->gate->equals(n->gate->getFd()))) {
            vector<Node *> floor = tree->getNodes(node->profondeur);
            for (size_t idx = 0; idx < floor.size(); idx++)
                if (floor[idx]->gate->equals(node->gate))
                    coords.push_back(
                        make_tuple(node->getX(), node->getY(), idx));
        }
    }
}

void ArbreView::placeWire() {
    int i = 0, x = 0;
    vector<Node *> floor = tree->getNodes(i++);
    while (floor.size() != 0) {        
        vector<tuple<int, int, int>> coords;

        for (Node *n : floor)
            placeWireOfSons(n, coords, false);

        if (i == tree->profondeur || i == 1) {
            x -= 2;
        } else
            x -= (floor.size() * 2) + 2;
        floor = tree->getNodes(i++);
    }
}

void ArbreView::getConnectedInputs(Gate *g, vector<string> &list){
    if(g->getType() == 1)
        list.push_back(g->getLabel());
    
    if (g->getFg() != nullptr) getConnectedInputs(g->getFg(),list);
    if (g->getFd() != nullptr) getConnectedInputs(g->getFd(), list);
}

void ArbreView::colorizeInput(Gate *g, bool color){
    vector<string> list;
    getConnectedInputs(g,list);

    for(Gate *gate : tree->list_racine)
        gate->colorizeGates(list,color);
}

void ArbreView::colorizeGates(Gate *g, bool color) { 
    g->setColor(color);

    if (g->getFg() != nullptr) colorizeGates(g->getFg(), color);
    if (g->getFd() != nullptr) colorizeGates(g->getFd(), color);
}

void ArbreView::placeWireOfSons(Node *n, vector<tuple<int, int, int>> coords,
                                bool cursor) {
    if (n->gate->getType() != 1) {
        if (n->profondeur != 0) {
            int x1 = n->getX();
            int y1 = n->getY();
            getSonsCoord(n, coords);

            for (size_t j = 0; j < coords.size(); j++) {
                int x2 = get<0>(coords[j]);
                int y2 = get<1>(coords[j]);
                int l = 2 * get<2>(coords[j]) + 1 + x2;

                for (int idx = x1 - 1; idx > l;
                     idx--) {  // on bouge vers le haut
                    if (grid[idx][y1] == "*")
                        grid[idx][y1] = "+";
                    else if (grid[idx][y1] == " " && idx == l + 1)
                        grid[idx][y1] = "*";
                    else
                        grid[idx][y1] = "|";
                }

                // ici on est à la ligne l, on bouge vers la droite ou la gauche
                int idx = y1;
                while (idx != y2) {
                    if (idx > y2) idx--;
                    if (idx < y2) idx++;

                    if (grid[l + 1][idx] == "*") {
                        grid[l + 1][idx] = "+";
                    } else if (grid[l + 1][idx] == " " && idx == y2) {
                        grid[l + 1][idx] = "*";
                    } else {
                        grid[l + 1][idx] = "-";
                    }
                }

                for (int idx = l; idx > x2; idx--) {  // on bouge vers le haut
                    if (grid[idx][y2] == "*")
                        grid[idx][y2] = "+";
                    else
                        grid[idx][y2] = "|";
                }
            }
        } else
            grid[n->getX() - 1][n->getY() - 1] = "|";
    }
}

int ArbreView::getPositionOnLevel(Node *n) {
    vector<Node *> floor = tree->getNodes(current->profondeur);
    for (size_t i = 0; i < floor.size(); i++) {
        if (floor[i]->gate->equals(n->gate))
            return i;
    }
    return -1;
}

// gèrera le curseur
// gèrera les entrées en commande line de l'utilisateur
void ArbreView::moveCursor(char direction) {
    colorizeInput(current->gate, false);
    colorizeGates(current->gate, false);
    
    vector<Node *> floor;
    size_t i = getPositionOnLevel(current);
   
    if (direction == 'z') {
        floor = tree->getNodes(current->profondeur + 1);
        size_t idx = (i + floor.size()) / 2;
        if (floor.size() != 0){
            if (idx < floor.size())
                current = floor[idx];
            else
                current = floor[floor.size() - 1];
        }

    } else if (direction == 'q') {
        floor = tree->getNodes(current->profondeur);
        if (i != 0) current = floor[i - 1];
    } else if (direction == 's') {
        floor = tree->getNodes(current->profondeur - 1);
        size_t idx = (i + floor.size()) / 2;
        if (floor.size() != 0){
            if (idx < floor.size())
                current = floor[idx];
            else
                current = floor[floor.size() -1];
        }
    } else if (direction == 'd') {
        floor = tree->getNodes(current->profondeur);
         if (i != floor.size() - 1) current = floor[i + 1];
    }

    // on colorie les fils
    colorizeInput(current->gate, true);
    colorizeGates(current->gate, true);
    
    placeGate();
}

void ArbreView::display() const {
    for (size_t i = 0; i < grid.size(); i++) {
        for (size_t j = 0; j < grid[i].size(); j++) {
            cout << ((grid[i][j] != " ") ? grid[i][j] : " ");
            if (j == grid[i].size() - 1)
                 cout << endl;
        }
    }
    cout << endl;
}