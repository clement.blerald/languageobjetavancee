#include "ORGate.hpp"

ORGate::ORGate(Gate *fg, Gate *fd) : Gate2Input{fg,fd}{
    label = "OR";
    type = 6;
    if(fd != nullptr && fg != nullptr) setVal();
}

void ORGate::setVal(){
    val = (fg->getVal() == 1)  || ( fd->getVal() == 1 );
}