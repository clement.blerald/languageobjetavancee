#include <iostream>
#include <string>
#include "Gate2Input.hpp"

using namespace std;

#ifndef ANDGATE_HPP
#define ANDGATE_HPP

class ANDGate : public Gate2Input{
    public:
        ANDGate(Gate *fg, Gate *fd);
        void setVal() override;
        int getType() const override;
};



#endif