#include "OutputGate.hpp"

OutputGate::OutputGate(string m_label, Gate *m_f) : Gate1Input{m_f}{
    label = m_label;
    type = 7;
    if(fg != nullptr) setVal();
}

void OutputGate::setVal(){
    val = fg->getVal();
}

int OutputGate::getType() const{
    return type;
}