#include <iostream>
#include <string>
#include <vector>

#include "ParserFic.hpp"

using namespace std;

#ifndef ARBREVIEW_HPP
#define ARBREVIEW_HPP

class ArbreView {
    friend class Shell;
   public:
    ArbreView(Arbre * &m_tree);
    virtual ~ArbreView();
    void display() const;

   private:
    Arbre *tree;
    Node *current;
    vector<vector<string>> grid;
    int width,height,nbGateMax;
    void getSonsCoord(Node *n, vector<tuple<int,int,int>> &coords);

    Node* initCursorPosition();
    void setGrid();
    void fillGrid();
    void placeGate();
    void placeWire();
    void placeWireOfSons(Node *n, vector<tuple<int,int,int>> coords, bool cursor);
    int getPositionOnLevel(Node *n);
    void colorizeInput(Gate *g, bool color);
    void colorizeGates(Gate *g, bool color);
    void getConnectedInputs(Gate *g,vector<string> &list);
    void moveCursor(char direction);
};

#endif