#include "ANDGate.hpp"

ANDGate::ANDGate(Gate *fg, Gate *fd) : Gate2Input{fg, fd} {
    label = "AND";
    type = 0;
    if(fd != nullptr && fg != nullptr) setVal();
}

void ANDGate::setVal(){
    val = fg->getVal() && fd->getVal();
}

int ANDGate::getType() const{
    return type;
}
