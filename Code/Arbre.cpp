#include "Arbre.hpp"

Arbre::Arbre() {}

Arbre::Arbre(Gate *racine){
    this->profondeur = 0;
    list_racine.push_back(racine);
    createNode(racine);
    setDepth();
    setLabel_length();
}

void Arbre::createNode(Gate *gate){
    if(existingNode(gate) == nullptr)
        list_node.push_back(new Node(gate));
    if(gate->fg != nullptr) createNode(gate->fg);
    if(gate->fd != nullptr) createNode(gate->fd);
}

Arbre::~Arbre() {}

void Arbre::setDepth() {
    for (size_t i = 0; i < list_racine.size(); i++)
        setDepth_rec(list_racine[i], 0);
}

void Arbre::setDepth_rec(Gate *gate, int _profondeur) {
    if(this->profondeur < _profondeur) this->profondeur = _profondeur;
    for (size_t i = 0; i < list_node.size(); i++) {
        if (list_node[i]->gate->equals(gate) &&
            _profondeur > list_node[i]->profondeur) {
            list_node[i]->profondeur = _profondeur;
            break;
        }
    }
    if (gate->fg != nullptr) setDepth_rec(gate->fg, _profondeur + 1);
    if (gate->fd != nullptr) setDepth_rec(gate->fd, _profondeur + 1);
}

void Arbre::fillLabels() {
    for (Gate *gate : list_racine) gate->fillLabels(label_length);
}

void Arbre::setLabel_length() {
    size_t max = 0;
    for (Node *n : list_node)
        if (n->gate->getLabel().size() > max) max = n->gate->getLabel().size();

    label_length = max;
    fillLabels();
}

void Arbre::changeInputValue(string input_name, regex e) {
    vector<Gate*> vect;
    for (Gate *gate : list_racine) gate->changeInputValue(input_name, e,vect);
}

vector<Node *> Arbre::getNodes(int profondeur) {
    vector<Node *> ret;
    for (size_t i = 0; i < list_node.size(); i++)
        if (list_node[i]->profondeur == profondeur) ret.push_back(list_node[i]);
    return ret;
}

Gate *Arbre::existingNode(Gate *g) {
    for (size_t i = 0; i < list_node.size(); i++)
        if (list_node[i]->gate->equals(g)) return list_node[i]->gate;
    return nullptr;
}

void Arbre::MajVal() {
    for (Gate *gate : list_racine) gate->majVal();
}