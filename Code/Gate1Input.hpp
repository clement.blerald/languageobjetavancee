#include <iostream>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>

#include "Gate.hpp"

#ifndef GATE1INPUT_HPP
#define GATE1INPUT_HPP

class Gate1Input : public Gate{
    protected:
        Gate1Input(Gate *m_fg);
        virtual ~Gate1Input();
        int getType() const override;
        virtual void setVal() = 0;
        Gate * setSon(Gate * other) override;
};

#endif