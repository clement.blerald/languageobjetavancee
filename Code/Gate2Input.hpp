#include <iostream>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>

#include "Gate.hpp"

using namespace std;

#ifndef GATE2INPUT_HPP
#define GATE2INPUT_HPP

class Gate2Input : public Gate{
    protected:
        Gate2Input(Gate *m_fg, Gate *m_fd);
        int getType() const;
        virtual void setVal() = 0;
        Gate * setSon(Gate * other) override;
};

#endif