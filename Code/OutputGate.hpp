#include <iostream>
#include <string>
#include "Gate1Input.hpp"

using namespace std;

#ifndef OUTPUTGATE_HPP
#define OUTPUTGATE_HPP

class OutputGate : public Gate1Input{
    public:
        OutputGate(string label, Gate *f);
        void setVal() override;
        int getType() const override;
};



#endif