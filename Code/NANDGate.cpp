#include "NANDGate.hpp"

NANDGate::NANDGate(string label, Gate *fg, Gate *fd) : Gate2Input(fg,fd){
    label = "NAND";
    if(fd != nullptr && fg != nullptr) setVal();
    type = 2;
}

void NANDGate::setVal(){
    val = !(fg->getVal() && fd->getVal());
}