#include <iostream>
#include <string>
#include <vector>

#include "Gate.hpp"
#include "Node.hpp"

using namespace std;

#ifndef ARBRE_HPP
#define ARBRE_HPP

class Arbre {
   friend class ParserFic;
   friend class ParserCommandLine;
   friend class ArbreView;
   friend class Shell;
   
   public:
    Arbre();
    Arbre(Gate *gate);
    ~Arbre();
    
    vector<Gate *> list_racine;
    vector<Node *> list_node;

   private:
    

    int label_length;
    int profondeur;
    void setLabel_length();
    void fillLabels();
    void setDepth();  // fill list_node
    void setDepth_rec(Gate *gate, int profondeur);
    void createNode(Gate *gate);
    Gate* existingNode(Gate *gate);
    void MajVal();
    void changeInputValue(string input_label, regex e);
    vector<Node*> getNodes(int n); //fonction qui récupère dans un vector les Node de profondeur n
};



#endif