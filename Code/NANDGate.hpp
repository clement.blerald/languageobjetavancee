#include <iostream>
#include <string>
#include "Gate2Input.hpp"

#ifndef NANDGATE_HPP
#define NANDGATE_HPP

class NANDGate : public Gate2Input{
    public:
        NANDGate(string label, Gate *fg, Gate *fd);
        void setVal() override;
};



#endif